from dataclasses import dataclass


def twos(val, bits):
    if (val & (1 << (bits - 1))) != 0:  # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)  # compute negative value
    return val


def fmt_bin(bits):
    return "{:0" + str(bits) + "b}"


def calc_mantissa(e, val, m_bits):
    """the mantissa is a binary number of the form 1.xxxxxxxxxx, xs being 1s and 0s.
    Example: 0000000001 represents 1.0000000001 (binary)

    If the exponent has minimum value (all zero), special rules for
    denormalized values are followed. The exponent value is set
    to 2-126 and the "invisible" leading bit for the mantissa is no longer used.

    The range of the mantissa is now [0:1).
    """
    m_fmt = "{:0" + str(m_bits) + "b}"
    b = m_fmt.format(val)
    rv = 0.0 if e == 0 else 1.0
    for i, bit in enumerate(b):
        rv += int(bit) * 2 ** (-i - 1)
    return rv


def calc_exponent(exponent, bias):
    implicit = 1 if exponent == 0 else 0
    return exponent - bias + implicit


@dataclass
class Floating(object):
    """Returns function that can represent a floating point given mantissa and exponent"""

    m_bits: int
    e_bits: int

    def to_dec(self, s, e, m):
        bias = 2 ** (self.e_bits - 1) - 1
        e, m = calc_exponent(e, bias), calc_mantissa(e, m, self.m_bits)
        sign = -1 if s == 1 else 1
        return sign * m * 2 ** e

    def bin_to_dec(self, b):
        fmt = fmt_bin(1 + self.m_bits + self.e_bits)
        b = fmt.format(b)
        s, e, m = b[0], b[1:1 + self.e_bits], b[1 + self.e_bits:self.e_bits + self.m_bits + 1]
        return self.to_dec(*map(lambda x: int(x, 2), [s, e, m]))

    def print(self, s, m, e):
        e_fmt, m_fmt = fmt_bin(self.e_bits), fmt_bin(self.m_bits)
        fmt_str = "{}|" + e_fmt + "|" + m_fmt + " -> 2^{:d} * {:1.4f} -> {:1.8f}"
        print(fmt_str.format(s, e, m, e, m, self.to_dec(s, m, e)))


def minifloat_8():
    # http://www.cs.jhu.edu/~jorgev/cs333/readings/8-Bit_Floating_Point.pdf
    m_bits, e_bits = 4, 3
    f8 = Floating(m_bits=m_bits, e_bits=e_bits)

    for sign in (0, 1):
        for exponent in range(0, 2 ** e_bits):
            for mantissa in range(0, 2 ** m_bits):
                f8.print(sign, exponent, mantissa)


if __name__ == "__main__":
    f8 = Floating(e_bits=3, m_bits=4)
    print(f8.bin_to_dec(0b0_101_1110))
    # minifloat_8()
