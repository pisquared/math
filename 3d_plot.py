import matplotlib.pyplot as plt


def main():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X = [1, 2, 3, 4]
    Y = [1, 2, 3, 4]
    Z = [1, 2, 3, 4]
    ax.scatter(X, Y, Z, c='r', marker='o')
    plt.show()


if __name__ == "__main__":
    main()
