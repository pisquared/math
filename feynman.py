from copy import deepcopy
from itertools import combinations


class Particle(object):
    def __init__(self, is_own_anti=False):
        self.is_own_anti = is_own_anti

    def __repr__(self):
        return "{}".format(self.__class__.__name__)


class EChargedParticle(Particle): pass


class ElectroWeakParticle(Particle): pass


class MatterParticle(Particle): pass


class Boson(Particle): pass


class Hadron(MatterParticle): pass


class Baryon(Hadron): pass


class Meson(Hadron): pass


class Fermion(MatterParticle): pass


class Lepton(Fermion): pass


class Quark(Fermion, EChargedParticle): pass


class UptypeQuark(object): pass


class DowntypeQuark(object): pass


class Neutrino(object): pass


class UpQuark(Quark, UptypeQuark): pass


class DownQuark(Quark, DowntypeQuark): pass


class CharmQuark(Quark, UptypeQuark): pass


class StrangeQuark(Quark, DowntypeQuark): pass


class TopQuark(Quark, UptypeQuark): pass


class BottomQuark(Quark, DowntypeQuark): pass


class Electron(Lepton, EChargedParticle): pass


class Muon(Lepton, EChargedParticle): pass


class Taon(Lepton, EChargedParticle): pass


class ElectronNeutrino(Lepton, Neutrino): pass


class MuonNeutrino(Lepton, Neutrino): pass


class TaonNeutrino(Lepton, Neutrino): pass


class PhotonZ(Boson, ElectroWeakParticle): pass


class Photon(PhotonZ):
    def __init__(self):
        super().__init__(is_own_anti=True)


class Gluon(Boson):
    def __init__(self):
        super().__init__(is_own_anti=True)


class Wtype(Boson, EChargedParticle, ElectroWeakParticle): pass


class Wp(Wtype): pass


class Wm(Wtype): pass


class Z0(PhotonZ):
    def __init__(self):
        super().__init__(is_own_anti=True)


class Higgs(Boson):
    def __init__(self):
        super().__init__(is_own_anti=True)


class Proton(Baryon): pass


class Neutron(Baryon): pass


class InteractingParticle(object):
    def __init__(self, particle, anti: bool = False):
        self.particle = particle
        self.anti = anti

    def is_type(self, other):
        anti_match = self.anti == other.anti if other.particle.is_own_anti else True
        return anti_match and isinstance(self.particle, other.particle.__class__)

    def __repr__(self):
        return "{}{}".format("anti-" if self.anti else "", self.particle)


class DiagramPermutation(object):
    def __init__(self, incoming: list = None, outgoing: list = None):
        self.incoming = incoming or []
        self.outgoing = outgoing or []

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __repr__(self):
        incoming_str = " + ".join([repr(i) for i in self.incoming])
        outgoing_str = " + ".join([repr(o) for o in self.outgoing])
        return "{} -> {}".format(incoming_str, outgoing_str)


class Vertex(object):
    EMPTY = E = "E"

    def __init__(self, interacting: list, edges_count: int = 0):
        self.edges_count = edges_count
        self.edges = [self.E for _ in range(self.edges_count)]
        self.mid_point = len(self.edges) // 2

        for diagram_particle in interacting:
            self.edges[diagram_particle.edge] = diagram_particle.interacting_particle

    def mirror(self, l):
        return l[:self.mid_point][::-1] + l[self.mid_point:][::-1]

    def rotate(self, l):
        def flip(ip):
            if ip != self.EMPTY and not ip.particle.is_own_anti:
                ip = deepcopy(ip)
                ip.anti = not ip.anti
            return [ip]

        return flip(l[-1]) + l[0:self.mid_point - 1] + flip(l[self.mid_point - 1]) + l[self.mid_point:-1]

    def generate_rotations(self, diagram):
        rv = []
        for i in range(len(self.edges)):
            rv.append(DiagramPermutation(incoming=[p for p in diagram[self.mid_point:] if p != self.EMPTY],
                                         outgoing=[p for p in diagram[:self.mid_point] if p != self.EMPTY]))
            diagram = self.rotate(diagram)
        return rv

    def generate_perms(self, include_mirror=False):
        rv = self.generate_rotations(self.edges)
        if include_mirror:
            rv = set(set(rv) | set(self.generate_rotations(self.mirror(self.edges))))
            return rv
        return set(rv)


class VertexParticle(object):
    def __init__(self, interacting_particle: InteractingParticle, edge: int):
        self.interacting_particle = interacting_particle
        self.edge = edge


class Vertex0V(Vertex):
    def __init__(self, interacting: list):
        # I-shaped edges, Identity, no real vertex
        super().__init__(interacting, edges_count=2)


class Vertex3V(Vertex):
    def __init__(self, interacting: list):
        # Ж-shaped edges, counting starts from top-left clockwise
        super().__init__(interacting, edges_count=6)


class Vertex4V(Vertex):
    def __init__(self, interacting: list):
        # X-shaped edges, counting starts from top-left clockwise
        super().__init__(interacting, edges_count=4)


# All from https://en.wikipedia.org/wiki/Standard_Model#/media/File:Standard_Model_Feynman_Diagram_Vertices.png
identity_v = Vertex0V(interacting=[VertexParticle(InteractingParticle(Particle()), 0),
                                   VertexParticle(InteractingParticle(Particle()), 1)])
fermion_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(Fermion()), 4),
                                  VertexParticle(InteractingParticle(Fermion()), 0),
                                  VertexParticle(InteractingParticle(Photon()), 2),
                                  ])
charged_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(EChargedParticle()), 4),
                                  VertexParticle(InteractingParticle(EChargedParticle()), 0),
                                  VertexParticle(InteractingParticle(Photon()), 2),
                                  ])
quark_gluon_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(Quark()), 4),
                                      VertexParticle(InteractingParticle(Quark()), 0),
                                      VertexParticle(InteractingParticle(Gluon()), 2),
                                      ])
up_down_quark_w_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(UptypeQuark()), 4),
                                          VertexParticle(InteractingParticle(DowntypeQuark()), 0),
                                          VertexParticle(InteractingParticle(Wtype()), 2),
                                          ])
leptop_neutrino_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(Lepton()), 4),
                                          VertexParticle(InteractingParticle(Neutrino()), 0),
                                          VertexParticle(InteractingParticle(Wtype()), 2),
                                          ])
three_gluon_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(Gluon()), 4),
                                      VertexParticle(InteractingParticle(Gluon()), 0),
                                      VertexParticle(InteractingParticle(Gluon()), 2),
                                      ])
w_photonz_v = Vertex3V(interacting=[VertexParticle(InteractingParticle(Wp()), 4),
                                    VertexParticle(InteractingParticle(Wm()), 0),
                                    VertexParticle(InteractingParticle(PhotonZ()), 2),
                                    ])
ww_xy_v = Vertex4V(interacting=[VertexParticle(InteractingParticle(Wp()), 0),
                                VertexParticle(InteractingParticle(Wm()), 1),
                                VertexParticle(InteractingParticle(ElectroWeakParticle()), 2),
                                VertexParticle(InteractingParticle(ElectroWeakParticle()), 3),
                                ])
four_gluon_v = Vertex4V(interacting=[VertexParticle(InteractingParticle(Gluon()), 0),
                                     VertexParticle(InteractingParticle(Gluon()), 1),
                                     VertexParticle(InteractingParticle(Gluon()), 2),
                                     VertexParticle(InteractingParticle(Gluon()), 3),
                                     ])

VERTICES = [identity_v,
            fermion_v,
            # four_gluon_v,
            ]


class Interaction(object):
    def __init__(self, incoming: list = None, outgoing: list = None):
        self.incoming = incoming or []
        self.outgoing = outgoing or []

    def match_perm(self, perm, incoming, outgoing):
        if len(incoming) != len(perm.incoming) or len(outgoing) != len(perm.outgoing):
            return False
        for incoming_p, perm_i_p in zip(incoming, perm.incoming):
            if not incoming_p.is_type(perm_i_p):
                return False
        if outgoing:
            for outgoing_p, perm_o_p in zip(outgoing, perm.outgoing):
                if not outgoing_p.is_type(perm_o_p):
                    return False
        return True

    def calc_step(self, incoming, path, depth):
        step_matches = []
        is_last_step = depth == 0
        outgoing = self.outgoing if is_last_step else []
        # TODO: If it's last step all incoming must match with all outgoing
        incoming_combs = list(combinations(incoming, 1)) + list(combinations(incoming, 2))
        for incoming_c in incoming_combs:
            # TODO: incoming outside of incoming combos need to have an identity passthrough
            for diagram in VERTICES:
                perms = diagram.generate_perms(True)
                for perm in perms:
                    if self.match_perm(perm, incoming_c, outgoing):
                        step_matches.append(perm)
        if is_last_step:
            return step_matches
        for perm in step_matches:
            step_matches += self.calc_step(step_matches, path=path + [perm], depth=depth - 1)

    def get_matching_diagrams(self, steps=0):
        return self.calc_step(self.incoming, path=[], depth=steps)


if __name__ == "__main__":
    electron = InteractingParticle(Electron(), anti=False)
    positron = InteractingParticle(Electron(), anti=True)
    photon = InteractingParticle(Photon())
    e_p_annihilation = Interaction(incoming=[electron, positron],
                                   outgoing=[photon])
    print(e_p_annihilation.get_matching_diagrams())
