from math import *
from decimal import *

getcontext().prec = 9

c = Decimal('299_792_458')  # m*s^-1
h = Decimal('6.626_070_15e-34')  # Js
h_bar = Decimal(2 * pi) * Decimal('6.626_070_15e-34')  # Js
e = Decimal('1.602_176_634e-19')  # C
cs = Decimal('9_192_631_770')  # s-1
m_p = Decimal('1.672_621_911e-27')  # kg

# Electronvolt - the energy required to accelerate
# a fundamental unit of charge through a potential
# difference of one volt
eV = e  # J


# c = h/2pi = 1
# E = hc/x = 2*pi/x; x = 2*pi/E
def convert(unit, f, t):
    u = Decimal(unit)
    f, t = map(lambda x: x.strip().lower(), [f, t])
    conversions = [
        ("ev", "m", lambda x: (x * e) / (h * c)),
        ("ev", "kg", lambda x: e / (x * c ** 2)),
    ]
    for f_unit, t_unit, formula in conversions:
        if f_unit == f and t_unit == t:
            return formula(u)
        elif f_unit == t and t_unit == f:
            return Decimal(1) / formula(u)


def main():
    print("1 eV = {:.4E} m^-1".format(convert(1, "ev", "m")))
    print("proton mass (kg) = {:.4E} eV".format(convert(m_p, "kg", "ev")))
    print("Higgs = 125 GeV = {:.4E} kg".format(convert(125e9, "ev", "kg")))


if __name__ == "__main__":
    main()
