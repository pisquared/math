import sys
from dataclasses import dataclass, field
from random import randrange, choice
from time import sleep
from typing import List

E, F = "⬜", "⬛"
presets = dict(
    # Still life
    block=[(0, 0), (1, 0), (0, 1), (1, 1)],
    beehive=[(1, 0), (2, 0), (0, 1), (3, 1), (1, 2), (2, 2)],
    loaf=[(1, 0), (2, 0), (0, 1), (3, 1), (1, 2), (3, 2), (2, 3)],
    boat=[(0, 0), (1, 0), (0, 1), (2, 1), (1, 2)],

    # Oscillators
    blinker=[(1, 0), (1, 1), (1, 2)],
    toad=[(2, 0), (0, 1), (3, 1), (0, 2), (3, 2), (1, 3)],
    beacon=[(0, 0), (1, 0), (0, 1), (1, 1), (2, 2), (3, 2), (2, 3), (3, 3)],
    pd=[(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (0, 1), (2, 1), (3, 1), (4, 1), (5, 1), (7, 1), (0, 2),
        (1, 2), (2, 2), (3, 2), (4, 2), (5, 2), (6, 2), (7, 2)],

    # Spaceships
    glider=[(0, 0), (1, 1), (2, 1), (0, 2), (1, 2)],
    lwss=[(1, 0), (2, 0), (0, 1), (1, 1), (3, 1), (4, 1), (1, 2), (2, 2), (3, 2), (4, 2), (2, 3), (3, 3)],
    mwss=[(1, 0), (2, 0), (0, 1), (1, 1), (3, 1), (4, 1), (5, 1), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2), (2, 3), (3, 3), (4, 3)],
    hwss=[(1, 0), (2, 0), (0, 1), (1, 1), (3, 1), (4, 1), (5, 1), (6, 1), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2), (6, 2), (2, 3),
          (3, 3), (4, 3), (5, 3)],
)


def create_grid(x, y):
    return [[E for _ in range(x)] for _ in range(y)]


def clear_prev_line():
    sys.stdout.write("\033[F")  # back to previous line
    sys.stdout.write("\033[K")  # clear line


def print_grid(grid, clear=False):
    if clear:
        for _ in grid:
            clear_prev_line()
    for y in grid:
        for x in y:
            sys.stdout.write(x)
        sys.stdout.write('\n')


def set_value(grid, x, y, symbol):
    x_len, y_len = len(grid[0]), len(grid)
    grid[y % y_len][x % x_len] = symbol


def set_initials(grid, initials):
    for initial in initials:
        x, y = initial
        set_value(grid, x, y, F)


adjs = [(1, 0), (-1, 0),  # x-right, x-left
        (0, 1), (0, -1),  # y-down, y-up
        (-1, -1), (1, -1), (-1, 1), (1, 1)]  # diagonals


def compose_initials(presets):
    rv = []
    for preset in presets:
        preset_type, coords = preset
        for preset_coords in preset_type:
            rv.append(tuple(map(sum, zip(preset_coords, coords))))
    return rv


@dataclass
class CellularAutomata2D(object):
    """
    Generic Cellular Automata rule engine
    Conway's Life is Birth=[3], Sustain=[2,3]

    See others here: https://www.conwaylife.com/wiki/Cellular_automaton#Life-like_cellular_automata
    """
    B: List = field(default_factory=lambda: [3, ])
    S: List = field(default_factory=lambda: [2, 3])

    def step_grid(self, grid):
        x_len, y_len = len(grid[0]), len(grid)
        next_grid = create_grid(x_len, y_len)
        for y, col in enumerate(grid):
            for x, row in enumerate(col):
                alive, sq = 0, grid[y][x]
                # check for adjacent alive
                for adj in adjs:
                    adj_x, adj_y = adj
                    adj_sq = grid[(y + adj_y) % y_len][(x + adj_x) % x_len]
                    if adj_sq == F:
                        alive += 1
                if alive in self.B or (sq == F and alive in self.S):
                    set_value(next_grid, x, y, F)
        return next_grid


def play(grid_size, initials, step=False, sleep_time=0.1, gens=0, rulestring="B3/S23"):
    gen = 0
    grid = create_grid(*grid_size)
    set_initials(grid, initials)
    print_grid(grid)
    ca = CellularAutomata2D(*b_s_from_rulestring(rulestring))
    while True:
        if gens and gen > gens:
            break
        gen += 1
        print_grid(grid, clear=True)
        grid = ca.step_grid(grid)
        if step:
            input("{} > ".format(gen))
        else:
            print("{} gen".format(gen))
            sleep(sleep_time)
        clear_prev_line()


def b_s_from_rulestring(rulestring, **kwargs):
    # https://www.conwaylife.com/wiki/Cellular_automaton#Life-like_cellular_automata
    rulelist = rulestring.split('/')
    b = [int(i) for i in rulelist[0][1:]]
    s = [int(i) for i in rulelist[1][1:]]
    return [b, s]


def simulate_presets():
    g = 64
    preset_values = list(presets.values())
    while True:
        random_initials = [(choice(preset_values), (randrange(0, g), randrange(0, g))) for _ in range(randrange(5, 15))]
        initials = compose_initials(random_initials)
        play(
            grid_size=(g, g),
            initials=initials,
            sleep_time=0.05,
            gens=200,
        )
        for _ in range(64):
            clear_prev_line()


def simulate_random(rulestring="B3/S23"):
    g = 50
    while True:
        initials = [((randrange(0, g), randrange(0, g))) for _ in range(randrange(0, g * g))]
        play(
            rulestring=rulestring,
            grid_size=(g, g),
            initials=initials,
            sleep_time=0.05,
            gens=200,
        )
        for _ in range(g):
            clear_prev_line()


def simple():
    initials = compose_initials([
        (presets['blinker'], (7, 7)),
        (presets['glider'], (1, 1)),
        (presets['beacon'], (5, 5)),
    ])
    play(grid_size=(50, 50),
         initials=initials,
         step=False,
         sleep_time=0.05,
         )


if __name__ == "__main__":
    simulate_random()
