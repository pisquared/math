from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/create", methods=["POST"])
def create():
    payload = request.form
    keys = [tuple(map(int, k.split('_'))) for k in payload.keys()]
    return """
    <pre>{}</pre>
    """.format(keys)


def main():
    app.run(debug=True)


if __name__ == "__main__":
    main()
